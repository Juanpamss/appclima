//
//  GustViewController.swift
//  AppClima
//
//  Created by Juan Pa on 25/10/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import UIKit
import CoreLocation

class GustViewController: UIViewController, CLLocationManagerDelegate {

    //MARK:- Outlets
    
    @IBOutlet weak var weatherGuest: UILabel!
    
    let locationManager = CLLocationManager()
    
    
    //MARK:- Attributes
    
    //var bandera = false
    
    
    //MARK:- viewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Actions
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        let location = manager.location?.coordinate
        
        //if !bandera{
            
            consultarPorUbicacion(
                
                lat:(location?.latitude)!,
                lon:(location?.longitude)!
                
            )
        
            //bandera = true
            
        //}
        
        locationManager.stopUpdatingLocation()
        
    }
    
    private func consultarPorUbicacion(lat:Double, lon:Double){
        
        let service = Servicio()
        
        service.consultarPorUbicación(lat: lat, lon: lon) { (weather) in
            
            DispatchQueue.main.async {
                self.weatherGuest.text = weather
            }
            
        }
        
        
    }
   
}
    
