//
//  CiudadViewController.swift
//  AppClima
//
//  Created by Juan Pa on 31/10/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import UIKit

class CiudadViewController: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var textCiudad: UITextField!
    
    @IBOutlet weak var weatherResult: UILabel!
    
    
    //MARK:- ViewController lifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    //MARK:- Actions
    
    @IBAction func buttonConsultar(_ sender: Any) {
        
        let service = Servicio()
        
        service.consultarPorCiudad(city: textCiudad.text!) { (weather) in
            DispatchQueue.main.async {
                self.weatherResult.text = weather
            } 
        }
        
    }
    

}
