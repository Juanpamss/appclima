//
//  ViewController.swift
//  AppClima
//
//  Created by Juan Pa on 25/10/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Outlets

    @IBOutlet weak var textUser: UITextField!
    @IBOutlet weak var textPass: UITextField!
    
    //MARK:- ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        textUser.text = ""
        textPass.text = ""
        textUser.becomeFirstResponder()
    }
    
    //MARK:- Actions

    @IBAction func ButtonLog(_ sender: Any) {
        
        let user = textUser.text ?? ""
        let password = textPass.text ?? ""
        
        switch(user,password){
        case("sebas","sebas"):
            
            performSegue(withIdentifier: "CiudadSegue", sender: self)
            
        case("sebas", _):
            mostrarAlerta(mensaje: "Contraseña incorrecta")
            
        default:
            mostrarAlerta(mensaje: "Usuario y contraseña incorrectas")
            
        }
        
    }
    
    private func mostrarAlerta(mensaje: String){
        
        let alertView = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
        
        let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            self.textUser.text = ""                                                         //Agregar botones a las alertas
            self.textPass.text = ""
            self.textUser.becomeFirstResponder()
        }
        
        alertView.addAction(aceptar) //Agregando el boton 
        
        present(alertView, animated: true, completion: nil)
    }
    
    
    @IBAction func ButtonGuest(_ sender: Any) {
    }
}

