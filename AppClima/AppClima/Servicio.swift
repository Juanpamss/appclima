//
//  Servicio.swift
//  AppClima
//
//  Created by Juan Pa on 7/11/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import Foundation

class Servicio {
    
    func consultarPorCiudad(city:String, completion:@escaping (String)->()){
        
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=b58e5394483c502fffe590067ad2f7a4"
        consultar(urlStr: urlStr) { (weather) in
            completion(weather)
        }
        
    }
    
    func consultarPorUbicación(lat:Double, lon:Double, completion:@escaping (String)->()){
        
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=b58e5394483c502fffe590067ad2f7a4"
        consultar(urlStr: urlStr) { (weather) in
            completion(weather)
        }
        
    }
    
    func consultar(urlStr:String, completion:@escaping (String)->()){
        
        let url = URL(string: urlStr)
        
        let request = URLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let _ = error{
                
                return
            }
            
            do{
                let weatherJSON = try JSONSerialization.jsonObject(with: data!, options: [])
                
                let weatherDict = weatherJSON as! NSDictionary
                
                guard let weatherKey = weatherDict ["weather"] as? NSArray else {
                    
                    completion("Ciudad no valida")
                    
                    return
                }
                
                let weather = weatherKey[0] as! NSDictionary
                
                completion("\(weather["description"] as! String)")
                 
            }catch{
                
                print("Error al generar el JSON")
                
            }
         }
        
        task.resume()
        
    }
    
    
}

